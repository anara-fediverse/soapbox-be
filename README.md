# Soapbox BE

![Soapbox BE](https://gitlab.com/soapbox-pub/soapbox-be/uploads/288bc05ba65e60970ffddd37d58f7c21/be-1-0-thumb.png)

**Soapbox BE** is the preferred backend for Soapbox.
It is based on [Pleroma](https://pleroma.social/).

## Your social media server

Soapbox empowers people to take control of their social media experience.
Hosting your own server means that *you* get to decide the rules.

Soapbox connects to over 4,000 other servers on the Fediverse.
It is designed to spread your message far and wide, while being resilient to deplatforming.

## Installation

See [the installation guide](https://soapbox.pub/install/).

## License

Soapbox is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Soapbox is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Soapbox.  If not, see <https://www.gnu.org/licenses/>.
